﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Simulasi_Logic_Day_9
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Masukkan Pilihan Soal 1-10 = ");
            int pilihan = int.Parse(Console.ReadLine());

            switch (pilihan)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;
                case 6:
                    soal6();
                    break;
                case 7:
                    soal7();
                    break;
                case 8:
                    soal8();
                    break;
                case 9:
                    soal9();
                    break;
                case 10:
                    soal10();
                    break;

            }
            Console.ReadKey();
        }
        static void soal1()
        {
            string kata;
            string alphabetbesar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int output = 0;

            Console.Write("Masukkan Kata = ");
            kata = Console.ReadLine();
           
            for(int i = 0; i < kata.Length; i++)
                {
                    int index = alphabetbesar.IndexOf(kata[i]);
                    if (index > -1)
                    {
                        output += 1;
                    }
                }
            Console.Write(output);
        }// Setiap Huruf Besar Output Bertambah 1
        static void soal2()
        {
            int start;
            int end;

            string inisial_perusahaan = "XS";
            DateTime tanggalan = new DateTime(2022, 08, 07);
            int invoice = 00000;

            int tahun = tanggalan.Year;
            int bulan = tanggalan.Month;
            int tanggal = tanggalan.Day;

            Console.Write("Masukkan Inputan Angka Awal = ");
            start = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Inputan Angka Terakhir = ");
            end = int.Parse(Console.ReadLine());

            for(int i = start; i <= end; i++)
            {
                invoice = i;
                Console.WriteLine($"{inisial_perusahaan}-{tanggal}{bulan}{tahun}-{invoice}");
            }
        }// Input Invoice
        static void soal3()
        {

        }
        static void soal4()
        {

            Console.WriteLine("Menu :");
            Console.WriteLine("1.Laki-Laki Dewasa");
            Console.WriteLine("2.Wanita Dewasa");
            Console.WriteLine("3.Anak-Anak");
            Console.WriteLine("4.Bayi");

            int laki,anak,bayi;
            int input;
            bool ulangi;

            int wanita        = 0;
            int baju_laki     = 0;
            int baju_wanita   = 0;
            int baju_anak     = 0;
            int baju_bayi     = 0;
            int baju_tambahan = 0;
            int total_baju    = 0;

            while (ulangi = true)
            {
                Console.Write("Baju Untuk Menu No. = ");
                input = int.Parse(Console.ReadLine());
                switch (input)
                {
                    case 1:
                        Console.Write("Jumlah Baju Untuk Laki-Laki Dewasa = ");
                        laki = int.Parse(Console.ReadLine());
                        baju_laki = laki * 1;
                        total_baju += baju_laki;
                        break;
                    case 2:
                        Console.Write("Jumlah Baju Untuk Wanita Dewasa = ");
                        wanita = int.Parse(Console.ReadLine());
                        baju_wanita = wanita * 2;
                        total_baju += baju_wanita;
                        break;
                    case 3:
                        Console.Write("Jumlah Baju Untuk Anak = ");
                        anak = int.Parse(Console.ReadLine());
                        baju_anak = anak * 3;
                        total_baju += baju_anak;
                        break;
                    case 4:
                        Console.Write("Jumlah Baju Untuk bayi = ");
                        bayi = int.Parse(Console.ReadLine());
                        baju_bayi = bayi * 5;
                        total_baju += baju_bayi;
                        break;
                }
                Console.WriteLine($"\nUlangi Proses ? Y/N ");
                string proses = Console.ReadLine();
                if (proses.ToLower() == "y")
                {
                    ulangi = true;
                }
                else if (proses.ToLower() == "n")
                {
                    ulangi = false;
                    Console.WriteLine($"Baju Laki-Laki Dewasa = {baju_laki}");
                    Console.WriteLine($"Baju Wanita Dewasa    = {baju_wanita}");
                    Console.WriteLine($"Baju Anak-Anak        = {baju_anak}");
                    Console.WriteLine($"Baju Bayi             = {baju_bayi}");
                    if (total_baju % 2 == 1 && total_baju > 10)
                    {
                        baju_tambahan = total_baju + wanita;
                    }
                    else
                    {
                        baju_tambahan = total_baju; ;
                    }
                    Console.WriteLine($"Total Baju = {baju_tambahan}");
                    break;
                }
            }
        }// Perhitungan Baju
        static void soal5()
        {

        }
        static void soal6()
        {
            string hurufkecil = "abcdefghijklmnopqrstuvwxyz";
            string hurufbesar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            string inputan;

            Console.Write("Masukkan Inputan Kalimat = ");
            inputan = Console.ReadLine();

            for(int i =0; i < inputan.Length; i++)
            {
                int index = hurufkecil.IndexOf(inputan[i]);
                if( index == -1)
                {
                    index = hurufbesar.IndexOf(inputan[i]);
                }
            }
        }        
        static void soal7()
        {
            int input;
            int n1;
            int n2 = 1;
            int n3 = 1;
            Console.Write("Input Angka = ");
            input = int.Parse(Console.ReadLine());


            List<int> hasil = new List<int>();
            List<int> hasilgenap = new List<int>();
            List<int> hasilganjil = new List<int>();

            for (int i = 0; i < input; i++)
            {
                Console.Write(n2 + " ", n3 + " ");
                n1 = n2;
                n2 = n3;
                n3 = n1 + n2;
                if (n2 %2 == 0 || n3 %2 == 0)
                {
                    hasilgenap.Add(n2);
                    hasilgenap.Add(n3);
                }
                else if (n2 % 2 == 1 || n3 % 2 == 1)
                {
                    hasilganjil.Add(n2);
                    hasilganjil.Add(n3);
                }
            }
            Console.WriteLine();
            Console.WriteLine($"Output Genap ,Pertambahan = {hasilgenap.Sum()}, Average = {hasilgenap.Average()}");
            Console.WriteLine($"Output Ganjil Pertambahan = {hasilganjil.Sum()}, Average = {hasilganjil.Average()}");


        }
        static void soal8()
        {
        }
        static void soal9()
        {
            int pulsa = 0;
            int point = 0;

            Console.Write("Beli Pulsa =Rp.");
            pulsa = int.Parse(Console.ReadLine());

            List<int> total_point = new List<int>();

            if (pulsa >= 0 && pulsa <= 10000)
            {
                point += 0;
                total_point.Add(point);
            }
            else if (pulsa > 10000 && pulsa <= 30000)
            { 
            }
        }
        static void soal10()
        {

        }
    }
}
