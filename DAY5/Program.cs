﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAY5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Masukkan Pilihan Soal = ");
            int pilihan = int.Parse(Console.ReadLine());

            switch (pilihan)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;
                case 6:
                    soal6();
                    break; 
            }
            
            Console.ReadKey();
        }
        static void soal1()
        {
            string waktu;

            Console.Write("Masukkan Waktu = ");
            waktu = Console.ReadLine();
            int jam = int.Parse(waktu.Substring(0, 2));
            string aturanwaktu = waktu.Substring(8, 2);
            if (aturanwaktu == "PM")
            {
                jam += 12;
                if (jam >= 24)
                {
                    jam -= 24;
                    Console.Write("0");
                }
            }
            else if (aturanwaktu == "AM")
            {
                if(jam < 12)
                {
                    jam += 0;
                    Console.Write("0");
                }
                else if(jam == 12)
                {
                    jam -= 12;
                    Console.Write(+jam);
                }
            }
            Console.Write(jam + waktu.Substring(2, 6));
        }
        static void soal2()
        {
            int menu;
            int alergi;
            string harga_menu;
            int uang_elsa;

            Console.Write("Total Menu = ");
            menu = int.Parse(Console.ReadLine());
            Console.Write("Index Makanan Alergi = ");
            alergi = int.Parse(Console.ReadLine());
            Console.Write("Harga Menu = ");
            harga_menu = Console.ReadLine();
            string[] hargamenu = harga_menu.Split(',');
            Console.Write("Jumlah Uang Elsa = ");
            uang_elsa = int.Parse(Console.ReadLine());

            int[] harga = Array.ConvertAll(hargamenu, int.Parse);

            int sum = 0;
            for(int i=0; i< harga.Length; i++)
            {
                sum += harga[i];
            }
            int jumlah = sum - harga[alergi];
            int total = jumlah / 2;
            int sisauang = uang_elsa - total;

            Console.WriteLine();
            Console.WriteLine($"Elsa Harus Membayar =Rp. {total}");

            if(sisauang == 0)
            {
                Console.WriteLine("Uang Elsa Pas");
            }
            else
            {
                Console.WriteLine($"Sisa Uang Elsa Rp={sisauang}");
            }
        }
        static void soal3()
        {
            int[,] data = new int[,]
            {
                {11,2,4},
                {4,5,6 },
                {10,8,-12}
            };
            int temp = 0;
            int temp1 = 0;
            for(int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (i == j)
                    {
                        temp += data[i,j];
                    }
                    if (i == i-0-0 && j == 3-i-1  )
                    {
                        temp1 += data[i, j];
                    }
                }
            }
            Console.WriteLine($"Perbedaan Diagonal = {temp-temp1}");
        }
        static void soal4()
        {
            string lilin;
            int total = 0;

            Console.Write("Tinggi Lilin = ");
            lilin = Console.ReadLine();
            string[] Lilin = lilin.Split(' ');
            int[] ConvertLilin = Array.ConvertAll(Lilin, int.Parse);

            int tinggililin = ConvertLilin.Max();
            for (int i = 0; i < ConvertLilin.Length; i++)
            {
                if(tinggililin == ConvertLilin[i])
                {
                    total++;
                }
            }
            Console.Write($"Jumlah Lilin Yang Di Tiup {total}");

        }
        static void soal5()
        {
            int rotate;
            Console.Write("Input : ");
            string[] input = Console.ReadLine().Split(',');

            Console.Write("Masukan Rotate : ");
            rotate = int.Parse(Console.ReadLine());

            string temp = "";

            for (int j = 0; j< rotate; j++)
            {
                for (int i = 0; i < input.Length - 1; i++)
                {
                    temp = input[i];
                    input[i] = input[i + 1];
                    input[i + 1] = temp;

                }
                foreach (string s in input)
                {
                    Console.Write($"{s} ");
                }
                Console.WriteLine();
            }
        }
        static void soal6()
        {
            int rotate;
            Console.Write("Input : ");
            string[] input = Console.ReadLine().Split(',');

            Console.Write("Masukan Rotate : ");
            rotate = int.Parse(Console.ReadLine());

            string temp = "";

            for (int j = 0; j < rotate; j++)
            {
                for (int i = 0; i < input.Length - 1; i++)
                {
                    temp = input[0];
                    input[0] = input[i + 1];
                    input[i + 1] = temp;

                }
                foreach (string s in input)
                {
                    Console.Write($"{s} ");
                }
                Console.WriteLine();
            }
        }
    }
}
