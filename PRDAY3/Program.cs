﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRDAY3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Masukkan Soal PR = ");
            int pilihan = int.Parse(Console.ReadLine());

            switch (pilihan)
            {
                case 1:
                    soal1();
                break;
                case 2:
                    soal2();
                break;
                case 3:
                    soal3();
                break;
                case 4:
                    soal4();
                break;
                case 5:
                    soal5();
                break;
                case 6:
                    soal6();
                break;
            }
            Console.ReadKey();
        }
        static void soal1()
        {
            int angka,x;
            

            Console.Write("Input Angka = ");
            angka = int.Parse(Console.ReadLine());

            for(int i=2; i<=angka; i++)
            {
                while(angka % i == 0)
                {
                    x = angka / i;
                    Console.WriteLine($"{angka}/{i} = {x}");
                    angka = x;
                }
            }

        }
        static void soal2()
        {
            int angka = 1;
            int n = 5;
            for(int i = 1; i <= 5; i++)
            {
                for(int j=1; j <= 5; j++)
                {
                    if (i == 1)
                    {
                        Console.Write(angka + " ");
                        angka += 1;
                    }
                    else if (i == 5)
                    {
                        Console.Write(n + " ");
                        n -= 1;
                    }
                    else if (i == 2 && i == 3 && i == 4 || j == 1 || j == 5)
                    {
                        Console.Write("* ");
                    }
                    else
                        Console.Write("  ");
                }
                Console.Write("\n");
            }

        }
        static void soal3()
        {
            int angka = 3;
            int n = 7;

            for (int i = 1; i <= n; i++)
            {
                if (i % 2 == 0)
                {
                    Console.Write("* ");
                    continue;
                }
                else
                    Console.Write(angka + " ");
                angka *= 9;
            
            }
        }
        static void soal4()
        {
            int angka = 5;
            int n = 7;

            for (int i = 1; i <= n; i++)
            {
                if (i % 2 == 1)
                {    
                    Console.Write($"-{angka} "); 
                }
                else
                    Console.Write(angka + " ");
                    angka += 5;
            }
                
        }
        static void soal5()
        {
            int input;
            int n1;
            int n2 = 1;
            int n3 = 1;
            Console.Write("Input Angka = ");
            input = int.Parse(Console.ReadLine());

            for (int i= 1; i <= input; i++)
            {
                Console.Write(n2 + " ", n3 + " ");
                n1 = n2;
                n2 = n3;
                n3 = n1 + n2;
            }
            
        }
        static void soal6()
        {
            int input;
            int n1;
            int n2 = 1;
            int n3 = 1;
            int n4 = 1;

            Console.Write("Input Angka = ");
            input = int.Parse(Console.ReadLine());

            for(int i=0; i<= input; i++)
            {
                Console.Write(n2 + " ", n3 + " ", n4 + " ");
                n1 = n2;
                n2 = n3;
                n3 = n4;
                n4 = n1 + n2 + n3;
            }
        }
    }

}
