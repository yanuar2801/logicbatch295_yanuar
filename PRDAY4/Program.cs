﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRDAY4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Masukkan Pilihan Soal = ");
            int pilihan = int.Parse(Console.ReadLine());

            switch (pilihan)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
            }
            soal4();
            Console.ReadKey();
        }
        static void soal1()
        {
            string kalimat;

            Console.Write("Masukkan Kalimat = ");
            kalimat = Console.ReadLine();
            string[] katakata = kalimat.Split(' ');
            foreach (string kata in katakata)
            {
                for (int i = 0; i < kata.Length; i++)
                {
                    if (i == 0 || i == kata.Length - 1)
                    {
                        Console.Write(kata[i]);

                    }
                    else
                    {
                        Console.Write("*");
                    }
                }
                Console.Write(" ");
            }
        }
        static void soal2()
        {
            string kalimat;

            Console.Write("Masukkan Kalimat = ");
            kalimat = Console.ReadLine();
            string[] katakata = kalimat.Split(' ');
            foreach (string kata in katakata)
            {
                for (int i = 0; i < kata.Length; i++)
                {
                    if (i == 0 || i == kata.Length - 1)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(kata[i]);
                    }
                }
                Console.Write(" ");
            }
        }
        static void soal3()
        {
            string kata;

            Console.Write("Masukkan Kata/Kalimat = ");
            kata = Console.ReadLine();

            string kata_baru ="";
            for(int i =kata.Length-1; i>= 0; i--)
            {
                kata_baru += kata[i];

            }
            if(kata_baru==kata)
            {
                Console.Write("Benar");
            }
            else
            {
                Console.Write("Salah");
            }
            
        }
        static void soal4()
        {
            Console.Write("Masukkan Uang Yang Dimiliki =");
            int input = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Harga Celana = ");
            String celana = Console.ReadLine();
            String[] arrayCelana = celana.Split(',');
            int[] Celana = Array.ConvertAll(arrayCelana, int.Parse);

            Console.Write("Masukkan Harga Baju = ");
            String baju = Console.ReadLine();
            String[] arrayBaju = baju.Split(',');
            int[] Baju = Array.ConvertAll(arrayBaju, int.Parse);


            int[] jumlah = new int[Baju.Length];
            int max = 0;

            for (int i = 0; i < Baju.Length; i++)
            {
                int temp = (Baju[i] + Celana[i]);

                if (temp <= input)
                {
                    jumlah[i] = temp;
                    //if (temp > max)
                    //{
                    //    max = temp;
                    //}
                }
            }
            Console.Write($"Kamu dapat membeli baju dan celana dengan harga {jumlah.Max()}");

        }
    }
}
