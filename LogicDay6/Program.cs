﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicDay6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Masukkan Soal = ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    coba();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;
            }
            Console.ReadKey();
        }
            static void soal1()
            {
            
            string perjalanan;

            Console.Write("Masukkan Perjalanan = ");
            perjalanan = Console.ReadLine();
            

            int lembah = 0;
            int batas = 0;

            for (int i = 0; i < perjalanan.Length; i++) // perulangan untuk mengecek setiap kata yg di input pada perjalanan
            {
                string kata = perjalanan[i].ToString(); // varibel kata bisa menggunakan char, jika menggunakan string harus di convert

                if (kata == "D") // jika menggunakan char hanya menggunakan kutip 1 ''
                {
                    batas -=1;
                }
                else
                {
                    batas++;
                }
                if (kata == "D" && batas == -1)
                {
                    lembah++;
                }
            }
            Console.Write(lembah);
        }// Mencari Berapa Jumlah Lembah yang Dilalui
            static void soal2()
            {
                string alphabetkecil = "abcdefghijklmnopqrstuvwxyz";
                string alphabetbesar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                string kalimatbaru = "";
                string Alphabetkecilbaru = "";
                string Alphabetbesarbaru = "";
                int rotate;

                Console.Write("Masukan Kalimat = ");
                string kalimat = Console.ReadLine().ToLower();
                Console.Write("Masukan Jumlah Rotate = ");
                rotate = int.Parse(Console.ReadLine());

                string simpan = alphabetkecil;
                string simpanup = alphabetbesar;

                for (int i = 0; i < rotate; i++) // perulangan untuk rotasi agar huruf depan menjadi di belakang sesuai jumlah rotasi yg diinput
                {
                    Alphabetkecilbaru = simpan.Substring(1, simpan.Length - 1) + simpan[0];
                    simpan = Alphabetkecilbaru;

                    Alphabetbesarbaru = simpanup.Substring(1, simpanup.Length - 1) + simpanup[0];
                    simpanup = Alphabetbesarbaru;
                }

                //for (int i = 0; i < rotate; i++)
                //{
                    
                //}

                foreach (char huruf in kalimat)
                {
                    int indeks = alphabetkecil.IndexOf(huruf); // mencari huruf ada di indeks berapa di alphabetkecil
                    int indeksup = alphabetbesar.IndexOf(huruf);//mencari huruf ada di indeks berapa di alphabetbesar

                if (indeks != -1) // untuk kondisi huruf kecil
                    {
                        kalimatbaru += Alphabetkecilbaru[indeks];
                    }
                    else if (indeksup != -1) // untuk kondisi huruf besaar
                    {
                        kalimatbaru += Alphabetbesarbaru[indeksup];
                    }
                    else // untuk kondisi dimana jika huruf tidak ada di huruf kecil dan huruf besar
                    {
                        kalimatbaru += huruf;
                    }
                }
                Console.WriteLine($"Alphabet = {alphabetkecil}");
                Console.WriteLine($"Alphabet Baru = {Alphabetkecilbaru}");
                Console.WriteLine($"Kalimat Baru = {kalimatbaru}");
        }// Enkripsi Kata menggunakan sandi caesar
            static void soal3()
            {
            Console.Write("Input Tinggi: ");
            int[] tinggi = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);// deteksi soal itu adalah angka, maka convert array, kemudian di split agar mendapatkan indexnya, dan di parse karena di awal sifatnya console input itu string
            Console.Write("input Text Kalimat : ");
            string textKalimat = Console.ReadLine().ToLower();

            //buat deteksi untuk huruf yang nantinya akan di input dan di detec dengan data yang tersedia
            string alfabet = "abcdefghijklmnopqrstuvwxyz";
            // buat tempat penyimpanan dengan jenis List untuk inputan huruf baru
            List<int> simpan = new List<int>();

            //BUAT PERULANGAN UNTUK CEK TEXT KALIMAT AGAR INDEXNYA KEDETEKSI PADA TABEL STRING ALFABET YANG TERSEDIA
            for (int i = 0; i < textKalimat.Length; i++)
            {
                char huruf = textKalimat[i];//UNTUK detek dari inputan textKalimat pada index ke [i] di buat dalam variabel HURUF DENGAN JENIS CHAR
                int indeks = alfabet.IndexOf(huruf); // untuk membuat deteksi BARU dari hasil ALFABET YG TERSEDIA DAN DIDETEKSI INDEXNYA MILIK SI HURUF yang bersifat char tadi
                int nilai = tinggi[indeks];// ini untuk deteksi di setiap INDEKX PADA TINGGI 
                simpan.Add(nilai);

                //cetak
                Console.WriteLine($"Index Alfabet {huruf} = {indeks}, maka index ke {indeks} di elemen tinggi bernilai = {nilai}");
        }

            //buat elemen baru lagi untuk deteksi PANJANG/Length dan nilai MAX
            int lenght = textKalimat.Length;
            int max = simpan.Max();
            // cetak Hasil
            int hasil = max * lenght;
            Console.WriteLine($"{max} x {lenght} = {hasil}");
            //string tidak memerlukan perhitungan, jika perlu perhitungan WAJIB DIUBAH KE INT


            }// PDF No.3 
            static void soal4()
            {
            string vokal = "aiueoAIUEO";
            string konsonan = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";

            Console.Write("Input = ");
            string input = Console.ReadLine().ToLower();
            int x;
            int nilai = 0;

            Console.Write("\nHuruf Vokal = ");
            string[] arrayInput1 = new string[input.Length];
            for (x = 0; x < input.Length; x++)
            {
                nilai = vokal.IndexOf(input[x]);
                if (nilai > -1)
                {
                    arrayInput1[x] = input[x].ToString();
                }
            }
            Array.Sort(arrayInput1);
            foreach (string hasil in arrayInput1)
            {
                Console.Write(hasil);
            }
            Console.WriteLine();

            Console.Write("\nHuruf Konsonan = ");
            string[] arrayInput2 = new string[input.Length];
            for (x = 0; x < input.Length; x++)
            {
                nilai = konsonan.IndexOf(input[x]);
                if (nilai > -1)
                {
                    arrayInput2[x] = input[x].ToString();
                }
            }
            Array.Sort(arrayInput2);
            foreach (string hasil in arrayInput2)
            {
                Console.Write(hasil);
            }
            Console.WriteLine();
        }// Huruf Konsonan dan Huruf Vokal
            static void soal5()
            {

            string password;
            int i;

            Console.Write("Password : ");
            password = Console.ReadLine();

            int nilai1 = 0;
            int nilai2 = 0;
            int nilai3 = 0;
            int nilai4 = 0;

            for (i = 65; i <= 90; i++)
            {
                if (password.Contains((char)i))
                {
                    nilai1++;
                }
            }
            for (i = 97; i <= 122; i++)
            {
                if (password.Contains((char)i))
                {
                    nilai2++;
                }
            }
            for (i = 48; i <= 57; i++)
            {
                if (password.Contains((char)i))
                {
                    nilai3++;
                }
            }
            for (i = 32; i <= 47; i++)
            {
                if (password.Contains((char)i))
                {
                    nilai4++;
                }
            }
            for (i = 58; i <= 64; i++)
            {
                if (password.Contains((char)i))
                {
                    nilai4++;
                }
            }
            Console.Write("Output = \n ");
            if (password.Length < 6)
            {
                Console.WriteLine("Password Weak & Kurang dari 6 digit ");
                Console.WriteLine("Password Weak & Kurang symbol ");
            }
            else
            {
                if (nilai1 > 0 && nilai2 > 0 && nilai3 > 0 && nilai4 > 0)
                {
                    Console.WriteLine("Password Strong");
                }
                else
                {
                    Console.WriteLine("Password Weak");
                }
            }
            }// Membuat Password
            static void coba()
            {
            Console.Write("input kata = ");
            string kata = Console.ReadLine().ToLower();
            string cek = "hackerrank";
            string hasil = "";
            for (int i = 0; i < cek.Length; i++)
            {
                for (int j = 0; j < kata.Length; j++)
                {
                    int index = cek.IndexOf(kata[j]);
                    if (index > -1 && cek[i] == kata[j])
                    {
                        hasil += kata[j];
                        kata = kata.Remove(j, 1);
                        break;
                    }
                }
            }
            if (hasil == cek)
            {
                Console.WriteLine("YES");
            }
            else
                Console.WriteLine("NO");
        }
    }
}

