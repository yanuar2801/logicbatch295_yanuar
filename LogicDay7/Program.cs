﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicDay7
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Masukkan Pilihan Soal = ");
            int pilihan = int.Parse(Console.ReadLine());

            switch (pilihan)
            {
                case 1:
                     soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;
                case 6:
                    soal6();
                    break;
                case 7:
                    soal7();
                    break;
                case 8:
                    soal8();
                    break;
            }
            Console.ReadKey();
        }
        static void date() // Membuat Date Time
        {
            DateTime date = new DateTime();
            Console.WriteLine(date);

            DateTime date1 = new DateTime(2022, 07, 08);
            Console.WriteLine(date1);

            DateTime dat2 = DateTime.Now;
            Console.WriteLine(dat2);

            int tahun = dat2.Year;
            int menit = dat2.Minute;
            int bulan = dat2.Month;
            int hari = dat2.Day;
        }
        static void datetimeparsing() // mengubah dari string menjadi Date
        {
           // string dateString = "06/30/22";

            Console.Write("Masukkan Tanggal = ");
            string tanggal = Console.ReadLine();
            DateTime datetime2 = DateTime.Parse(tanggal);
            Console.Write(datetime2.ToString("dd MMMM yyyy")); // untuk mengubah format nya menjadi dari Hari Bulan Laptop
        }
        static void datetimeproperties()
        {
            //Console.Write("Masukkan Tanggal = ");
            //string tanggal = Console.ReadLine();
            //DateTime dateinput = DateTime.Parse(tanggal);
            
            DateTime dat2 = DateTime.Now;
            Console.WriteLine(dat2);

            int tahun = dat2.Year;
            int menit = dat2.Minute;
            int bulan = dat2.Month;
            var hari = dat2.DayOfWeek;
            int hari1 = (int) dat2.DayOfWeek;

            //TimeSpan interval = dateinput - dat2; // mencari jarak waktu

            Console.WriteLine(hari1);
            dat2 = dat2.AddDays(1); //untuk menambahkan hari
            dat2 = dat2.AddDays(-1); //untuk mengurangi hari
            //Console.Write(interval);
        }
        static void soal1()
        {
            Console.Write("Masukkan Tanggal dan Jam Masuk = ");
            string jam_masuk = Console.ReadLine();
            DateTime masuk = DateTime.Parse(jam_masuk);

            Console.Write("Masukkan Tanggal dan Jam Keluar = ");
            string jam_keluar = Console.ReadLine();
            DateTime keluar = DateTime.Parse(jam_keluar);

            TimeSpan interval = keluar - masuk;

            int hasil = interval.Hours * 3000;

            Console.Write($"Biaya Parkir = Rp{hasil}");

        } // Harga Tiket Karcis Tower Satrio
        static void soal2()
        {

            Console.Write("Masukkan Tanggal Peminjaman Buku = ");
            string tanggal = Console.ReadLine();
            DateTime meminjam = DateTime.Parse(tanggal);

            Console.Write("Masukkan Tanggal Pengembalian Buku = ");
            string pengembalian = Console.ReadLine();
            DateTime balikin = DateTime.Parse(pengembalian);

            int totalHarga = 0;

            if (balikin > meminjam)
            {
                TimeSpan jarak = balikin - meminjam.AddDays(3);
                int Jarak = (int)jarak.TotalDays;
                totalHarga = Jarak * 500;
                Console.WriteLine($"{totalHarga}");
            }
            else
            {
                Console.Write("Terimakasih Karena Telah Mengembalikan Buku Dengan Tepat Waktu");
            }
        } // Pinjaman Buku
        static void soal3()
        {
            Console.Write("Masukkan Tanggal Masuk : ");
            string tanggalMasuk = Console.ReadLine();
            DateTime mulai = DateTime.Parse(tanggalMasuk);

            Console.Write("Masukan Tanggal Libur : ");
            string[] libur = Console.ReadLine().Split(',');

            for (int i = 1; i <= 11; i++)
            {
                int date = mulai.Day;
                int hari = (int)mulai.DayOfWeek;
                if (hari == 6 || hari == 0)
                {
                    i--;
                }
                else if (Array.IndexOf(libur, date.ToString()) != -1)
                {
                    i--;
                }
                else
                    Console.WriteLine($"Tanggal {date} hari ke {i}.");
                if (i == 11)
                {
                    break;
                }
                mulai = mulai.AddDays(1);
            }
            Console.WriteLine($"Kelas akan ujian pada {mulai}");
        } // Tanggall Mulai Bootcamp
        static void soal4()
        {
            int durasi_main;
            int harga;
            int jam;
            int tambahan_billing = 0;
            bool nambah;

            Console.Write("Masukkan Durasi Sewa Warnet = ");
            durasi_main = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Jam Mulai Main = ");
            string jam_masuk = Console.ReadLine();
            DateTime masuk = DateTime.Parse(jam_masuk);

            Console.Write($"\n Ingin Tambah Billing (Ya/Tidak) =  ");
            string input = Console.ReadLine();
            if (input.ToLower() == "ya")
            {
                nambah = true;
                Console.Write("Masukkan Tambahan Billing = ");
                tambahan_billing = int.Parse(Console.ReadLine());

            }
            else if (input.ToLower() == "tidak")
            {
                nambah = false;
                tambahan_billing += 0;
            }
            else
            {
                Console.WriteLine("Anda Memasukan kode yang salah");
                nambah = false;
            }
            jam = masuk.Hour + durasi_main + tambahan_billing;
            harga = (durasi_main + tambahan_billing) * 3500;

            Console.WriteLine($"Durasi Sewa Akan Habis Pada Pukul {jam}:" + masuk.Minute);
            Console.WriteLine($"Total Harga nya Adalah = Rp.{harga}");

        } // Billing warnet
        static void soal5()
        {
            Console.Write("Input lokasi ke : ");
            int lokasi = int.Parse(Console.ReadLine());

            double customer1 = 2;
            double customer2 = 0.5;
            double customer3 = 1.5;
            double customer4 = 0.3;

            double hasil = 0;

            if (lokasi == 1)
            {
                hasil = customer1;
                Console.WriteLine($"Jarak Tempuh = {hasil} KM");
            }
            else if (lokasi == 2)
            {
                hasil = customer1 + customer2;
                Console.WriteLine($"Jarak Tempuh = {customer1} KM +{customer2 * 1000} M = {hasil} KM");
            }
            else if (lokasi == 3)
            {
                hasil = customer1 + customer2 + customer3;
                Console.WriteLine($"Jarak Tempuh = {customer1} KM + {customer2 * 1000} M + {customer3} KM = {hasil} KM");
            }
            else if (lokasi == 4)
            {
                hasil = customer1 + customer2 + customer3 + customer4;
                Console.WriteLine($"Jarak Tempuh = {customer1} KM + {customer2 * 1000} KM + {customer3} KM + {customer4 * 1000} M = {hasil} KM");
            }
            double liter = hasil / 2.5;

            double jmlLiter = Math.Ceiling(liter);
            Console.WriteLine($"Anda menghabiskan bensin sebanyak {jmlLiter} liter");
        } // Ojek Online
        static void soal6()
        {
            int faktorial;
            int total = 1;

            Console.Write("Masukkan Faktorial = ");
            faktorial = int.Parse(Console.ReadLine());

            for(int i =1; i <= faktorial; i++)
            {
                total *= i;
            }
            Console.Write($"Ada {total} Cara");
        } // Faktorial
        static void soal7()
        {
            string yeah;
            bool ulangg = true;
            int ulang1 = 0;

            Console.Write("Masukan Point                    = "); //poin taruhannya untuk menebak
            int point = int.Parse(Console.ReadLine());
            do
            {
                while (ulangg)
                {
                    Console.WriteLine($"Sisa Poin Anda                   = {point}");
                    Console.Write("Masang Taruhan                   = ");
                    int masangTaruhan = int.Parse(Console.ReadLine());
                    Console.Write("Masukan Jawaban Tebakan Anda U/D = ");
                    string answerSure = Console.ReadLine().ToUpper();
                    Random randomAngka = new Random(); // Fungsi Method Random tipe data angka di komputer
                    int numbers = randomAngka.Next(0, 9); // angka random mulai dari .Next(ini, sampai sini) yang telah disediakan/ sudah di deteksi                   
                    string password = "";
                    if (point > 0)
                    {
                        if (masangTaruhan <= point)
                        {
                            if (numbers > 5)
                            {
                                password = "U";
                            }
                            else if (numbers < 5)
                            {
                                password = "D";
                            }
                            else //if (numbers == 5 /*&& answersure == password && answersure != password*//*)
                            {
                                point += 0;
                                Console.WriteLine("\nSERI");
                            }


                            if (answerSure == password && numbers != 5)
                            {
                                point += masangTaruhan;
                                Console.WriteLine("\nYou Win");
                            }
                            else if (answerSure != password && numbers != 5)
                            {
                                point -= masangTaruhan;
                                Console.WriteLine("\nYou Lose");
                            }
                        }
                        else
                        {
                            Console.WriteLine($"Anda tidak bisa masang taruhan lebih dari = {point}");
                        }

                        Console.WriteLine($"Poin Yang Keluar Adalah {numbers}");
                    }
                    if (point <= 0)
                    {
                        point = 0;
                        Console.WriteLine("Game Over");
                    }
                    Console.WriteLine($"Point Saat ini                   = {point}");
                    ulang1 = point;// ini untuk perulangan WHILE

                    Console.WriteLine("Mau Bertaruh Lagi Pecundang? Y/N");
                    yeah = Console.ReadLine().ToUpper();
                    if (yeah == "Y")
                    {
                        ulangg = true;
                        Console.Clear();
                    }
                    else
                    {
                        ulangg = false;
                    }
                }
            }
            while (ulang1 > 0);
        } // Tebak Permainan
        static void soal8()
        {
            Console.Write("Input Kayu    : ");
            int[] inputarray = Array.ConvertAll(Console.ReadLine().Split(','), int.Parse);
            List<int> input = new List<int>(inputarray);//cara ubah array ke list

            input.Sort();
            List<int> hasil = new List<int>();
            List<int> hasiltemp = new List<int>();
            hasil.Add(input.Count);
            hasiltemp.Add(1);

            while (input.Count != 0)
            {
                int min = input.Min();
                for (int i = 0; i < input.Count; i++)
                {
                    int temp = input[i] - min;
                    input[i] = temp;

                    if (temp != 0)
                    {
                        hasiltemp.Add(temp);
                    }
                    else if (temp == 0)
                    {
                        input.Remove(temp);
                        i--;
                    }
                }
                if (input.Count != 0)
                {
                    hasil.Add(input.Count);
                }
                hasiltemp = new List<int>();
            }
            Console.WriteLine(string.Join(" ", hasil));
        } // Potong Kayu
    }   
}
