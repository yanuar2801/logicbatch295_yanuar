﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soal_6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Masukkan Perjalanan = ");
            string perjalanan = Console.ReadLine();

            int lembah = 0;
            int gunung = 0;
            int batas = 0;


            for (int i = 0; i < perjalanan.Length; i++) 
            {
                string kata = perjalanan[i].ToString().ToUpper(); 
                if (kata == "N") 
                {
                    batas += 1;
                }
                else if (kata == "T")
                {
                    batas -=1;
                }
                if (kata == "T" && batas == 0)
                {
                    gunung+=1;
                }
                else if (kata =="N" && batas == 0)
                {
                    lembah += 1;
                }
            }
            Console.WriteLine($"Jumlah Lembah yang dilewati = {lembah}");
            Console.Write($"Jumlah Gunung yang dilewati = {gunung}");

            Console.ReadKey();
        }
    }
}
