﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soal_9
{
    internal class Program
    {
        static void Main(string[] args)
        {
            
            Console.Write("Masukkan Jumlah Cupcake Yang ingin Dibuat = ");
            int cupcake = int.Parse(Console.ReadLine());

            decimal ter = 125;
            decimal gul = 100;
            decimal su = 100;

            decimal terigu = Math.Ceiling(ter / 15);
            decimal gula_pasir = Math.Ceiling(gul / 15);
            decimal susu = Math.Ceiling(su / 15);

            decimal a = 0;
            decimal b = 0;
            decimal c = 0;

            for (int i = 1; i < cupcake; i++)
            {
                a += terigu;
                b += gula_pasir;
                c += susu;
            }

            Console.WriteLine($"Terigu = {a}");
            Console.WriteLine($"Gula Pasir = {b}");
            Console.WriteLine($"Susu = {c}");

            Console.ReadKey();
        }
    }
}
