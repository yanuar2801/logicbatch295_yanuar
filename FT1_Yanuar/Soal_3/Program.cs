﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soal_3
{   
    internal class Program
    {
        static void Main(string[] args)
        {
            int pertama = 1;
            int kedua = 1;
            int ketiga = 0;

            Console.Write("Input x = ");
            int x = int.Parse(Console.ReadLine());

            int output = 0;

            for (int i= 0; i <= x ; i++)
            {
                ketiga = pertama + kedua;
                Console.Write(ketiga+ " ");
                if (ketiga < x && ketiga % 2 == 0 && ketiga > 0)
                {
                    output += 1;
                }
                pertama = kedua;
                kedua = ketiga;
                ketiga = pertama + kedua;
            }

            Console.WriteLine($"Output = {output} ");

            Console.ReadKey();
        }
    }
}
