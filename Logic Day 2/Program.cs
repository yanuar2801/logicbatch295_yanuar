﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic_Day_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("===================Logic Day 2===================");
            Console.WriteLine("SILAHKAN ANDA PILIH SOAL NYA (1/2/3/4/5/6/7/8) = ");
            int pilihan = int.Parse(Console.ReadLine());
            switch (pilihan)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;
                case 6:
                    soal6();
                    break;
                case 7:
                    soal7();
                    break;
                case 8:
                    soal8();
                    break;
            
            }


            Console.ReadKey();
        }
        static void soal1()
        {
            int nilai;

            Console.WriteLine("===========SOAL 1===========");
            Console.Write("Masukkan Nilai Anda = ");
            nilai = int.Parse(Console.ReadLine());

            if (nilai >= 85)
            {
                Console.WriteLine("Anda Mendapatkan Nilai A");
            }
            else if (nilai >= 70)
            {
                Console.WriteLine("Anda Mendapatkan Nilai B");
            }
            else

                Console.WriteLine("Anda Mendapatkan Nilai C");

        }
        static void soal2()
        {
            int pulsa;

            Console.WriteLine("===========SOAL 2============");
            Console.Write("Anda Membeli Pulsa Sejumlah = Rp.");
            pulsa = int.Parse(Console.ReadLine());

            if (pulsa < 25000)
            {
                Console.WriteLine($"Pulsa    : {pulsa}");
                Console.WriteLine($"Point    : 80     ");
            }
            else if (pulsa < 50000 && pulsa >= 25000)
            {
                Console.WriteLine($"Pulsa    : {pulsa} ");
                Console.WriteLine($"Point    :  200    ");
            }
            else if (pulsa < 100000 && pulsa >= 50000)
            {
                Console.WriteLine($"Pulsa    : {pulsa}");
                Console.WriteLine($"Point    :  400   ");
            }
            else
            {
                Console.WriteLine($"Pulsa    : {pulsa}");
                Console.WriteLine($"Point    :  800   ");
            }
        }
        static void soal3()
        {
            int total_belanja;
            int diskon;
            int jarak;
            int ongkir = 5000;
            string promo = "JKTOVO";
            int tambahanongkir;
            int jumlahongkir;

            Console.WriteLine("===========SOAL 3===========");
            Console.Write("Masukkan Total Belanja = Rp.");
            total_belanja = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Jarak         =  ","KM");
            jarak = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Kode PROMO    =  ");
            promo = Console.ReadLine();

            tambahanongkir = (jarak - 5) * 1000;

            if (total_belanja >= 30000)
            {
                diskon = total_belanja * 40 / 100;
                
                if (jarak >= 5)
                {
                    jumlahongkir = ongkir + tambahanongkir;
                    Console.WriteLine("--------------------");
                    Console.WriteLine($"Total Belanjaan Anda adalah Rp.{total_belanja}");
                    Console.WriteLine($"Diskon 40%        = Rp.{diskon}");
                    Console.WriteLine($"Ongkir            = RP.{jumlahongkir}");
                    Console.WriteLine($"Total Keseluruhan = Rp.{total_belanja - diskon + jumlahongkir}");

                }
                else
                {
                    Console.WriteLine("--------------------");
                    Console.WriteLine($"Total Belanjaan Anda adalah Rp.{total_belanja}");
                    Console.WriteLine($"Diskon 40%        = Rp.{diskon}");
                    Console.WriteLine($"Ongkir            = RP.{ongkir}");
                    Console.WriteLine($"Total Keseluruhan = Rp.{total_belanja - diskon + ongkir}");

                }
            }
            else
            {
                Console.WriteLine("--------------------");
                Console.WriteLine($"Total Belanjaan Anda adalah Rp.{total_belanja}");
                Console.WriteLine($"Diskon 40%        = Rp.0");
                Console.WriteLine($"Ongkir            = RP.{ongkir}");
                Console.WriteLine($"Total Keseluruhan = Rp.{total_belanja + ongkir}");

            }
        }
        static void soal4()
        {
            int belanja,totalbelanja;
            int ongkir,totalongkir;
            int voucher;

            Console.WriteLine("=============================SOAL 4===============================");
            Console.WriteLine("1.Min Order 30rb free ongkir 5rb dan potongan harga belanja 5rb   ");
            Console.WriteLine("2.Min Order 50rb free ongkir 10rb dan potongan harga belanja 10rb ");
            Console.WriteLine("3.Min Order 100rb free ongkir 20rb dan potongan harga belanja 10rb");
            Console.WriteLine("===================================================================");
            Console.Write("Masukan Total Belanja = Rp.");
            belanja = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Ongkos Kirim = Rp.");
            ongkir = int.Parse(Console.ReadLine());
            Console.Write("Pilih Voucher (1/2/3) = ");
            voucher = int.Parse(Console.ReadLine());

            switch (voucher)
            {
                case 1:
                    if (belanja >= 30000)
                    {
                        totalongkir = ongkir - 5000;
                        totalbelanja = belanja - 5000;

                        Console.WriteLine($"Belanja       = {belanja}");
                        Console.WriteLine($"Ongkos Kirim  = {ongkir}");
                        Console.WriteLine("Diskon Ongkir  = Rp.5000");
                        Console.WriteLine("Diskon Belanja = Rp.5000");
                        Console.WriteLine($"Total Belanja = {totalbelanja + totalongkir}");
                    }
                    else
                    {
                        Console.WriteLine($"Belanja       = {belanja}");
                        Console.WriteLine($"Ongkos Kirim  = {ongkir}");
                        Console.WriteLine("Diskon Ongkir  = Rp.-");
                        Console.WriteLine("Diskon Belanja = Rp.-");
                        Console.WriteLine($"Total Belanja = {belanja + ongkir}");
                    }
                    break;
                case 2:
                    if (belanja >= 50000)
                    {
                        totalongkir = ongkir - 10000;
                        totalbelanja = belanja - 10000;

                        Console.WriteLine($"Belanja       = {belanja}");
                        Console.WriteLine($"Ongkos Kirim  = {ongkir}");
                        Console.WriteLine("Diskon Ongkir  = Rp.10000");
                        Console.WriteLine("Diskon Belanja = Rp.10000");
                        Console.WriteLine($"Total Belanja = {totalbelanja + totalongkir}");
                    }
                    else
                    {
                        Console.WriteLine($"Belanja       = {belanja}");
                        Console.WriteLine($"Ongkos Kirim  = {ongkir}");
                        Console.WriteLine("Diskon Ongkir  = Rp.-");
                        Console.WriteLine("Diskon Belanja = Rp.-");
                        Console.WriteLine($"Total Belanja = {belanja + ongkir}");

                    }
                    break;
                case 3:
                    if (belanja >= 100000)
                    {
                        totalongkir = ongkir - 20000;
                        totalbelanja = belanja - 10000;

                        Console.WriteLine($"Belanja       = {belanja}");
                        Console.WriteLine($"Ongkos Kirim  = {ongkir}");
                        Console.WriteLine("Diskon Ongkir  = Rp.20000");
                        Console.WriteLine("Diskon Belanja = Rp.10000");
                        Console.WriteLine($"Total Belanja = {totalbelanja + totalongkir}");

                    }
                    else
                    {
                        Console.WriteLine($"Belanja       = {belanja}");
                        Console.WriteLine($"Ongkos Kirim  = {ongkir}");
                        Console.WriteLine("Diskon Ongkir  = Rp.-");
                        Console.WriteLine("Diskon Belanja = Rp.-");
                        Console.WriteLine($"Total Belanja = {belanja + ongkir}");

                    }
                    break;
            }

         }
        static void soal5()
        {
            string nama;
            int tahun_lahir;
            Console.WriteLine("================SOAL 5==================");
            Console.WriteLine("1. Baby Boomer , Kelahiran 1944 s/d 1964");
            Console.WriteLine("2. Generasi X , Kelahiran 1965 s/d 1979");
            Console.WriteLine("3. Generasi Y , Kelahiran 1980 s/d 1994");
            Console.WriteLine("4. Generasi Z , Kelahiran 1995 s/d 2015");

            Console.Write("Masukkan Nama Anda   = ");
            nama = Console.ReadLine();
            Console.Write("Masukkan Tahun Lahir = ");
            tahun_lahir = int.Parse(Console.ReadLine());

            if(tahun_lahir >= 1944 && tahun_lahir <=1964 )
            {
                Console.WriteLine($"{nama}, Berdasarkan Tahun Kelahiran Anda Tergolong Generasi Boomer");
            }
            else if (tahun_lahir >=1965 && tahun_lahir<=1979)
            {
                Console.WriteLine($"{nama}, Berdasarkan Tahun Kelahiran Anda Tergolong Generasi X");
            }
            else if(tahun_lahir >=1980 && tahun_lahir <= 1994)
            {
                Console.WriteLine($"{nama}, Berdasarkan Tahun Kelahiran Anda Tergolong Generasi Y");
            }
            else
            {
                Console.WriteLine($"{nama}, Berdasarkan Tahun Kelahiran Anda Tergolong Generasi Z");
            }
        }
        static void soal6()
        {
            int tunjangan, gapok;
            double banyak_bulan;
            double pajak, bpjs, gaji, total_gaji;
            double Pajak;
            double pajak1 = 0.05;
            double pajak2 = 0.10;
            double pajak3 = 0.15;
            double Bpjs = 0.03;
            string nama;

            Console.WriteLine("===========SOAL 6===========");
            Console.Write("Masukkan Nama Anda      = ");
            nama = Console.ReadLine();
            Console.Write("Masukkan Tunjangan Anda = ");
            tunjangan = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Gaji pokok Anda=");
            gapok = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Banyak Bulan   = ");
            banyak_bulan = double.Parse(Console.ReadLine());

            pajak = gapok + tunjangan;
            if (pajak <= 5000000)
            {
                
                Pajak = pajak1 * pajak;
                bpjs = Bpjs * pajak;
                gaji = pajak - Pajak - bpjs;
                total_gaji = (pajak - Pajak - bpjs) * banyak_bulan;
                Console.WriteLine($"Karyawan Atas Nama {nama} Slip Gaji Anda Sebagai Berikut");
                Console.WriteLine($"Pajak      = Rp.{Pajak}");
                Console.WriteLine($"BPJS       = Rp.{bpjs}");
                Console.WriteLine($"Gaji/Bulan = Rp.{gaji}");
                Console.WriteLine($"Total Gaji = Rp.{total_gaji}");
            }
            else if (pajak > 5000000 && pajak <= 10000000)
            {
                
                Pajak = pajak2 * pajak;
                bpjs = Bpjs * pajak;
                gaji = pajak - Pajak - bpjs;
                total_gaji = (pajak - Pajak - bpjs) * banyak_bulan;
                Console.WriteLine($"Karyawan Atas Nama {nama} Slip Gaji Anda Sebagai Berikut");
                Console.WriteLine($"Pajak      = Rp.{Pajak}");
                Console.WriteLine($"BPJS       = Rp.{bpjs}");
                Console.WriteLine($"Gaji/Bulan = Rp.{gaji}");
                Console.WriteLine($"Total Gaji = Rp.{total_gaji}");
            }
            else
            {
                
                Pajak = pajak3 * pajak;
                bpjs = Bpjs * pajak;
                gaji = pajak - Pajak - bpjs;
                total_gaji = (pajak - Pajak - bpjs) * banyak_bulan;
                Console.WriteLine($"Karyawan Atas Nama {nama} Slip Gaji Anda Sebagai Berikut");
                Console.WriteLine($"Pajak      = Rp.{Pajak}");
                Console.WriteLine($"BPJS       = Rp.{bpjs}");
                Console.WriteLine($"Gaji/Bulan = Rp.{gaji}");
                Console.WriteLine($"Total Gaji = Rp.{total_gaji}");
            }
        }    
        static void soal7()
        {
            int berat_badan, tinggi_badan;
            double bmi;
            double convert_tinggi;

            Console.WriteLine("===============SOAL 7===============");
            Console.WriteLine("Masukkan Berat Badan Anda (KG)  = ");
            berat_badan = int.Parse(Console.ReadLine());
            Console.WriteLine("Masukkan Tinggi Badan Anda (CM) = ");
            tinggi_badan = int.Parse(Console.ReadLine());

            convert_tinggi = tinggi_badan * 0.01;
            bmi = berat_badan / (convert_tinggi * convert_tinggi);
            Console.WriteLine($"Nilai BMI Anda Adalah {bmi}");
            if(bmi<18.5)
            {
                Console.WriteLine("Anda Termasuk Terlalu Kurus");

            }
            else if (bmi >= 18.5 && bmi <= 25)
            {
                Console.WriteLine("Anda Termasuk Langsing/Sehat");
            }
            else
            {
                Console.WriteLine("Anda Termasuk Gemuk");
            }
        }
        static void soal8()
        {
            int mtk, fisika, kimia;
            decimal rata_rata;
            Console.WriteLine("===========SOAL 8===========");
            Console.WriteLine("Masukkan Nilai MTK anda    = ");
            mtk = int.Parse(Console.ReadLine());
            Console.WriteLine("Masukkan Nilai Fisika anda = ");
            fisika = int.Parse(Console.ReadLine());
            Console.WriteLine("Masukkan Nilai Kimia anda  = ");
            kimia = int.Parse(Console.ReadLine());

            rata_rata = (mtk + fisika + kimia) / 3;
            Console.WriteLine($"Nilai Rata-Rata Anda adalah {rata_rata}");
            if (rata_rata > 50 )
            {
                Console.WriteLine("Selamat");
                Console.WriteLine("Kamu Berhasil");
                Console.WriteLine("Kamu Hebat");
            }
            else
            {
                Console.WriteLine("Maaf");
                Console.WriteLine("Kamu Gagal");
            }
        }
    }
 }    
        

        

