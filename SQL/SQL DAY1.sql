create database DBPenerbit

create table tblPengarang (
	id int primary key identity(1,1),
	kd_pengarang varchar (7) not null,
	nama varchar (30) not null,
	alamat varchar (80)not null,
	kota varchar (15)not null,
	kelamin varchar (1) not null
)
insert into tblPengarang(kd_pengarang,nama,alamat,kota,kelamin) values
('P0001','Ashadi','Jl.Beo 25','Yogya','P'),
('P0002','Rian','Jl.Solo 123','Yogya','P'),
('P0003','Suwadi','Jl.Semangka 13','Bandung','P'),
('P0004','Siti','Jl.Durian 15','Solo','W'),
('P0005','Amir','Jl.Gajah 33','Kudus','P'),
('P0006','Suparman','Jl.Harimau 25','Jakarta','P'),
('P0007','Jaja','Jl.Singa 7','Bandung','P'),
('P0008','Saman','Jl.Naga 12','Yogya','P'),
('P0009','Anwar','Jl.Tidar 6A','Magelang','P'),
('P0010','Fatmawati','Jl.Renjan 4','Bogor','W')

select * from tblPengarang

create table tblGaji (
	id int primary key identity (1,1),
	Kd_pengarang varchar (7) not null,
	nama varchar (30) not null,
	gaji decimal (18,4) not null
)

insert into tblGaji (Kd_pengarang,nama,gaji) values
('P0002','Rian',600000),
('P0005','Amir',700000),
('P0002','Siti',500000),
('P0002','Suwadi',1000000),
('P0002','Fatmawati',600000),
('P0002','Saman',600000)

select * from tblGaji

update tblGaji set gaji = 750000 where nama = 'Saman'
update tblGaji set Kd_pengarang = 'P0004' where id = 3
update tblGaji set Kd_pengarang = 'P0003' where id = 4
update tblGaji set Kd_pengarang = 'P0010' where id = 5
update tblGaji set Kd_pengarang = 'P0008' where id = 6

select count(nama) as Jumlah_Pengarang from tblPengarang -- No.1

select count(kelamin) as Gender,kelamin 
from tblPengarang 
group by kelamin -- No.2

select count(kota) as Jumlah_kota,kota 
from tblPengarang 
group by kota-- No.3

select count(kota) as Jumlah_kota, kota
from tblPengarang
group by kota 
having count(kota) > 1 --No.4

select kd_pengarang from tblPengarang where kd_pengarang = 'P0001' OR kd_pengarang = 'P0010'--No.5

select MIN(gaji) as Gaji_Terendah,MAX(gaji) as Gaji_Tertinggi from tblGaji --No.6

select gaji from tblGaji 
where gaji > 600000 --No.7

select SUM(gaji) from tblGaji --No.8

select p.kota,sum(g.gaji)as Gaji from tblPengarang as p 
join tblGaji as g  
on p.kd_pengarang = g.Kd_pengarang 
group by p.kota --No.9


select * from tblPengarang where id >= 3 and id <=6 --No.10

select * from tblPengarang where kota = 'Yogya' or kota = 'Solo' or kota = 'Magelang'--No.11

select * from tblPengarang where kota != 'Yogya' --No.12

select * from tblPengarang where nama like 'A%'
select * from tblPengarang where nama like '%i'
select * from tblPengarang where nama like '__a%'
select * from tblPengarang where nama not like '%n'
--No. 13

select * from tblPengarang as p -- p.*,g.*
join tblGaji as g
on p.kd_pengarang = g.Kd_pengarang --No.14
	
select kota as Kota,gaji as Gaji from tblPengarang as p 
join tblGaji as g
on p.kd_pengarang = g.Kd_pengarang
where gaji < 1000000 --No.15

alter table tblPengarang alter column kelamin varchar (10) --No.16

alter table tblPengarang add gelar varchar (12) -- No.17

update tblPengarang set alamat = 'Jl.Cendrawasih 65', kota = 'Pekanbaru' where nama = 'Rian' --No.18
update tblPengarang set kota ='Yogya' where nama = 'Rian'
select * from tblPengarang

CREATE VIEW vwPengarang AS
select Kd_pengarang,nama,kota from tblPengarang

select * from vwPengarang --No.19

update tblPengarang set gelar = 'S1'

select * from tblPengarang