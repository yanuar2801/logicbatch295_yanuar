create database DB_UNIX_XA

create table tbl_mahasiswa (
	nim int primary key,
	nama varchar (50),
	jenis_kelamin varchar (20),
	alamat varchar (50)
)

insert into tbl_mahasiswa(nim,nama,jenis_kelamin,alamat) values
(101,'Arif','L','Jl.Kenangan'),
(102,'Budi','L','Jl.Jombang'),
(103,'Wati','P','Jl.Surabaya'),
(104,'Ika','P','Jl.Jombang'),
(105,'Tono','L','Jl.Jakarta'),
(106,'Iwan','L','Jl.Bandung'),
(107,'Sari','P','Jl.Malang')

select*from tbl_mahasiswa

create table tbl_matkul (
	id int primary key identity (1,1),
	kode_mk varchar (10),
	nama varchar (50),
	sks int,
	semester int
)

insert into tbl_matkul (kode_mk,nama,sks,semester) values
('PTI447','Praktikum Basis Data',1,3),
('TIK342','Praktikum Basis Data',1,3),
('PTI333','Basis Data Terdistribusi',3,5),
('TIK123','Jaringan Komputer',2,5),
('TIK333','Sistem Operasi',3,5),
('PTI123','Grafika Multimedia',3,5),
('PTI777','Sistem Operasi',2,3)

select*from tbl_matkul

create table tb_ambil (
	nim int,
	kode_mk varchar (10)
)

insert into tb_ambil (nim,kode_mk) values
(101,'PTI447'),
(103,'TIK333'),
(104,'PTI333'),
(104,'PTI777'),
(111,'PTI123'),
(123,'PTI999')

select*from tb_ambil

--No.1
select m.nama,mat.nama
from tbl_mahasiswa as m
join tb_ambil as a on m.nim = a.nim
join tbl_matkul as mat on mat.kode_mk = a.kode_mk

--No.2

select m.nim,m.nama,m.jenis_kelamin,m.alamat 
from tbl_mahasiswa as m
left join tb_ambil as a on m.nim = a.nim
left join tbl_matkul as mat on mat.kode_mk = a.kode_mk
where a.nim is null  

--No.3

select count(m.nim) as Jumlah,jenis_kelamin
from tbl_mahasiswa as m
left join tb_ambil as a on m.nim = a.nim
left join tbl_matkul as mat on mat.kode_mk = a.kode_mk
where a.nim is null
group by jenis_kelamin

--No.4

select m.nim,m.nama,mat.kode_mk,mat.nama
from tbl_mahasiswa as m
left join tb_ambil as a  on m.nim = a.nim
left join tbl_matkul as mat on mat.kode_mk = a.kode_mk
where a.nim is not null

--No.5

select m.nim,m.nama,sum(mat.sks) as jmlh_sks
from tbl_mahasiswa as m
join tb_ambil as a on m.nim = a.nim
join tbl_matkul as mat on  mat.kode_mk = a.kode_mk
group by m.nim,m.nama
having sum(mat.sks) > 4

--No.6
select mat.nama 
from tbl_mahasiswa as m
join tb_ambil as a on m.nim = a.nim
right join tbl_matkul as mat on mat.kode_mk = a.kode_mk
where m.nama is null

