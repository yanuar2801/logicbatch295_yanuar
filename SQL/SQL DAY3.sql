create database DB_HR

create table tb_karyawan(
	id int primary key identity(1,1),
	id_ bigint not null,
	nip varchar (50) not null,
	nama_depan varchar (50) not null,
	nama_belakang varchar (50) not null,
	jenis_kelamin varchar (50) not null,
	agama varchar (50) not null,
	tempat_lahir varchar (50) not null,
	tgl_lahir date not null,
	alamat varchar (100) not null,
	pendidikan_terakhir varchar (50) not null,
	tgl_masuk date not null
)

insert into tb_karyawan ( id_,nip,nama_depan,nama_belakang,jenis_kelamin,agama,tempat_lahir,tgl_lahir,alamat,pendidikan_terakhir,tgl_masuk) values
(1,'001','Hamidi', 'Samsudin','Pria','Islam','Sukabumi','1977-04-21','Jl.Sudirman  No.12','S1 Teknik Mesin','2015-12-07'),
(3,'003','Paul' , 'Christian','Pria','Kristen','Ambon','1980-05-27','Jl.Veteran No.4','S1 Pendidikan Geografi','2014-01-12'),
(2,'002','Ghandi', 'Warmida','Wanita','Islam','Palu','1992-01-12','Jl.Rambutan No.22','SMA Negeri 02 Palu','2014-12-01')

select*from tb_karyawan

create table divisi (
	id int primary key identity(1,1),
	kd_divisi varchar (50) not null,
	nama_divisi varchar (50) not null
)

insert into divisi (kd_divisi,nama_divisi) values
('GD','Gudang'),
('HRD','HRD'),
('KU','Keuangan'),
('UM','Umum')

select*from divisi

create table jabatan (
	id int primary key identity (1,1),
	kd_jabatan varchar (50) not null,
	nama_jabatan varchar (50) not null,
	gaji_pokok numeric,
	tunjangan_jabatan numeric
)

insert into jabatan(kd_jabatan,nama_jabatan,gaji_pokok,tunjangan_jabatan) values
('MGR','Manager',5500000,1500000),
('OB','Office Boy',1900000,200000),
('ST','Staff',3000000,750000),
('WMGR','Wakil Manager',4000000,1200000)

select*from jabatan

create table pekerjaan(
	id int primary key identity (1,1),
	nip varchar (50),
	kode_jabatan varchar (50),
	kode_divisi varchar (50),
	tunjangan_kinerja varchar (50),
	kota_penempatan varchar (50)
)

insert into pekerjaan (nip,kode_jabatan,kode_divisi,tunjangan_kinerja,kota_penempatan) values
('001','ST','KU',750000,'Cianjur'),
('002','OB','UM',350000,'Sukabumi'),
('003','MGR','HRD',1500000,'Cianjur')

update pekerjaan set kota_penempatan = 'Sukabumi' where nip = '003' 
select*from pekerjaan

--No.1
select concat(k.nama_depan,k.nama_belakang) as Nama_Lengkap,j.nama_jabatan,
j.tunjangan_jabatan+j.gaji_pokok as gaji_tunjangan
from tb_karyawan as k
join pekerjaan as p on k.nip = p.nip
join jabatan as j on p.kode_jabatan = j.kd_jabatan
where j.tunjangan_jabatan+j.gaji_pokok < 5000000

--No.2

select CONCAT(k.nama_depan,k.nama_belakang) as Nama_Lengkap,
j.nama_jabatan, d.nama_divisi,
j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja as Total_Gaji,
(j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)*0.05 as Pajak,
(j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)-((j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)*0.05)
from tb_karyawan as k 
join pekerjaan as p on k.nip = p.nip
join jabatan as j on p.kode_jabatan = j.kd_jabatan
join divisi as d on p.kode_divisi = d.kd_divisi
where k.jenis_kelamin = 'Pria' AND p.kota_penempatan != 'Sukabumi'

--No.3

select k.nip,concat(nama_depan,nama_belakang) as Nama_Lengkap,j.nama_jabatan,d.nama_divisi,
(((j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)*7)*0.25)as Bonus
from tb_karyawan as k 
join pekerjaan as p on k.nip = p.nip
join jabatan as j on p.kode_jabatan = j.kd_jabatan
join divisi as d  on d.kd_divisi = p.kode_divisi

--No.4

select k.nip,concat(nama_depan,nama_belakang) as Nama_lengkap, j.nama_jabatan,d.nama_divisi,
j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja as Total_Gaji,
((j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)*0.5) as Infak
from tb_karyawan as k
join pekerjaan as p on k.nip = p.nip
join jabatan as j on p.kode_jabatan = j.kd_jabatan
join divisi as d on p.kode_divisi = d.kd_divisi
where p.kode_jabatan = 'MGR'

--No.5

select k.nip,concat(nama_depan,nama_belakang) as Nama_Lengkap,k.pendidikan_terakhir,2000000 as tunjangan_pendidikan,
(j.gaji_pokok+j.tunjangan_jabatan+2000000) as Total_Gaji  
from tb_karyawan as k
join pekerjaan as p on k.nip = p.nip
join jabatan as j on p.kode_jabatan = j.kd_jabatan
join divisi as d on p.kode_divisi = d.kd_divisi
where k.pendidikan_terakhir like '%S1%'

--No.6

select k.nip,CONCAT(nama_depan,nama_belakang) as Nama_Lengkap, j.nama_jabatan,d.nama_divisi,
case 
	when j.kd_jabatan = 'MGR'
	THEN (((j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)*7)*0.25)
	when j.kd_jabatan = 'ST'
	THEN (((j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)*5)*0.25)
	else (((j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)*2)*0.25)
end bonus
from tb_karyawan as k 
join pekerjaan as p on k.nip = p.nip
join jabatan as j on p.kode_jabatan = j.kd_jabatan
join divisi as d on p.kode_divisi = d.kd_divisi

--No.7

alter table tb_karyawan
add constraint UC_karyawan unique (nip)

--No.8

create unique index index_nip
on tb_karyawan(nip)

--No.9

select CONCAT(nama_depan,' ',UPPER(nama_belakang)) as Nama_Lengkap  
from tb_karyawan
where nama_belakang like 'w%'

--No.10

select CONCAT(nama_depan,' ',nama_belakang),tgl_masuk,j.nama_jabatan,d.nama_divisi,
j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja as Total_Gaji,
case
	when DATEDIFF(year,k.tgl_masuk,GETDATE()) >= 8
	then ((j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)*0.1)
	else (j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)
	end bonus,
DATEDIFF(year,k.tgl_masuk,GETDATE()) as Lama_kerja
from tb_karyawan as k
join pekerjaan as p on k.nip = p.nip
join jabatan as j on p.kode_jabatan = j.kd_jabatan
join divisi as d on p.kode_divisi = d.kd_divisi
where DATEDIFF(year,k.tgl_masuk,GETDATE()) >= 8

