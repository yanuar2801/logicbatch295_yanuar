create database DB_Entertainer

create table artis (
	id int primary key identity (1,1),
	kd_artis varchar (100)not null,
	nm_artis varchar (100)not null,
	jk varchar (100) not null,
	bayaran decimal (18,4)not null,
	award int not null,
	negara varchar (100) not null
)

insert into artis (kd_artis,nm_artis,jk,bayaran,award,negara) values
('A001','ROBERT DOWNEY JR','PRIA',3000000000,2,'AS'),
('A002','ANGELINA JOLIE','WANITA',700000000,1,'AS'),
('A003','JACKIE CHAN','PRIA',200000000,7,'HK'),
('A004','JOE TASLIM','PRIA',350000000,1,'ID'),
('A005','CHELSEA ISLAN','WANITA',300000000,0,'ID')

select * from artis

create table film (
	id int primary key identity (1,1),
	kd_film varchar (10) not null,
	nm_film varchar (55) not null,
	genre varchar (55) not null,
	artis varchar (55) not null,
	produser varchar (55) not null,
	pendapatan decimal (18,4) not null,
	nominasi int not null
)

insert into film (kd_film,nm_film,genre,artis,produser,pendapatan,nominasi) values
('FOO1','IRON MAN  ','G001','A001','PD01',2000000000,3),
('FOO2','IRON MAN 2','G001','A001','PD01',1800000000,2),
('FOO3','IRON MAN 3','G001','A001','PD01',1200000000,0),
('FOO4','AVENGER CIVIL WAR','G001','A001','PD01',2000000000,1),
('FOO5','SPIDERMAN HOME COMING','G001','A001','PD01',1300000000,0),
('FOO6','THE RAID','G001','A004','PD03',800000000,5),
('FOO7','FAST & FURIOUS','G001','A004','PD05',830000000,2),
('FOO8','HABIBIE DAN AINUN','G004','A005','PD03',670000000,4),
('FOO9','POLICE STORY','G001','A003','PD02',700000000,3),
('FO10','POLICE STORY 2','G001','A003','PD02',710000000,1),
('FO11','POLICE STORY 3','G001','A003','PD02',615000000,0),
('FO12','RUSH HOUR','G003','A003','PD05',695000000,2),
('FO13','KUNGFU PANDA','G003','A003','PD05',923000000,5)

select * from film

create table produser (
	id int primary key identity (1,1),
	kd_produser varchar (50) not null,
	nm_produser varchar (50)not null,
	international varchar (50) not null
)

insert into produser ( kd_produser,nm_produser,international) values
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA ','YA'),
('PD03','RAPI FILM','TIDAK'),
('PD04','PARKIT','TIDAK'),
('PD05','PARAMOUNT PICTURE','YA')

select * from produser

create table negara (
	id int primary key identity (1,1),
	kd_negara varchar (100) not null,
	nm_negara varchar (100) not null
)

insert into negara (kd_negara,nm_negara) values
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA')

select * from negara

create table genre (
	id int primary key identity (1,1),
	kd_genre varchar (50) not null,
	nm_genre varchar (50) not null
)

insert into genre (kd_genre,nm_genre) values
('G001','ACTION'),
('G002','HORROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRILLER'),
('G006','FICTION')

SELECT*FROM genre

--No.1
select p.nm_produser,sum(f.pendapatan) as pendapatan from film as f
join produser as p 
on f.produser = p.kd_produser
group by p.nm_produser 
having nm_produser= 'MARVEL'

--No.2
select nm_film,nominasi from film where nominasi = 0

--No.3
select top 1 nm_film,pendapatan from film

select nm_film,pendapatan from film where pendapatan = (select max(pendapatan) from film)

--No.4
select nm_film from film where nm_film like 'p%'

--No.5
select nm_film from film where nm_film like '%y'

--No.6
select nm_film from film where nm_film like '%d%'

--No.7
select f.nm_film,a.nm_artis 
from film as f
join artis as a 
on f.artis = a.kd_artis

--No.8
select f.nm_film,a.negara from film as f
join artis as a 
on f.artis = a.kd_artis
where a.negara = 'HK'

--No.9
select f.nm_film,n.nm_negara from film as f
join artis as a on f.artis = a.kd_artis
join negara as n on a.negara = n.kd_negara
where nm_negara not like '%o%'

--No.10
select a.nm_artis from film as f
right join artis as a 
on f.artis = a.kd_artis
where f.artis is null

--No.11
select a.nm_artis,g.nm_genre 
from film as f
join artis as a on f.artis = a.kd_artis
join genre as g on f.genre = g.kd_genre
where g.nm_genre = 'DRAMA'

--No.12
select a.nm_artis,g.nm_genre
from film as f
join artis as a on f.artis = a.kd_artis
join genre as g on f.genre = g.kd_genre
group by a.nm_artis,g.nm_genre
having g.nm_genre = 'ACTION'

--No.13
select n.kd_negara,n.nm_negara,count(f.nm_film) as jmlh_film
from negara as n 
left join artis as a on n.kd_negara = a.negara
left join film as f on a.kd_artis = f.artis
group by n.kd_negara,n.nm_negara

--No.14
select f.nm_film
from film as f 
join produser as p 
on f.produser = p.kd_produser
where p.international='YA'

--No.15
select p.nm_produser,count(f.nm_film) as jmlh_film 
from produser as p
left join film as f 
on p.kd_produser = f.produser
group by p.nm_produser

--Test Maju 

--No.7
select f.nm_film,a.nm_artis from film as f 
join artis as a 
on f.artis = a.kd_artis

--No.8
select f.nm_film,a.negara,n.nm_negara from film as f
join artis as a on f.artis = a.kd_artis
join negara as n on a.negara = n.kd_negara
where a.negara = 'HK'

--No.9
select f.nm_film, n.nm_negara from film as f
join artis as a on f.artis = a.kd_artis
join negara as n on a.negara = n.kd_negara
where n.nm_negara not like '%o%'


select*, 
case 
	when bayaran >= 500000000 then bayaran*0.2
	when bayaran < 500000000 AND bayaran > 200000000 then bayaran*0.1
	else  0
end as pajak,
case 
	when bayaran >=500000000 then bayaran - (bayaran*0.2)
	when bayaran < 500000000 AND bayaran > 200000000 then bayaran - (bayaran*0.2)
	else bayaran
end as Total_bayaran
from artis

select CONCAT( kd_artis,' ', nm_artis) from artis -- Untuk Menggabungkan Colom

select DATEPART(DAY,GETDATE()) as Tanggal
select Datename(weekday, GETDATE()) as Hari















