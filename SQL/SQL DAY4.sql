create database DB_Sales

create table salesperson(
		id int primary key identity (1,1),
		nama varchar (20),
		BOD date,
		salary decimal (18,2)
)

drop table salesperson
insert into salesperson (nama,BOD,salary) values
('Abe','1988/11/9',140000),
('Bob','1978/11/9',44000),
('Chris','1983/11/9',40000),
('Dan','1980/11/9',52000),
('Ken','1977/11/9',115000),
('Joe','1990/11/9',38000)

insert into salesperson (nama,BOD,salary) values
('abe','8-09-2020',2000)
insert into salesperson (nama,BOD,salary) values
('abe','9/11/1988',2000)
insert into salesperson (nama,BOD,salary) values
('abe','9/',2000)


select * from salesperson 

create table orders(
		id int primary key identity (1,1),
		order_date date,
		cust_id int,
		salesperson_id int,
		amount decimal(18,2)
)

insert into orders (order_date,cust_id,salesperson_id,amount) values
('2020/8/2',4,2,540),
('2021/1/22',4,5,1800),
('2019/7/14',9,1,460),
('2018/1/29',7,2,2400),
('2021/2/3',6,4,600),
('2020/3/2',6,4,720),
('2021/5/6',9,4,150)

select * from orders

select * from salesperson
select * from orders

--No.a
select s.nama, COUNT(o.salesperson_id) as Jumlah_orderan
from salesperson as s
join orders as o on s.id = o.salesperson_id
group by s.nama
having COUNT (o.salesperson_id) > 1

--No.b
select s.nama,sum(o.amount) as total_amount
from salesperson as s
join orders as o on s.id = o.salesperson_id
group by s.nama
having sum(o.amount) > 1000
order by sum(o.amount) asc
--No.c
select s.nama,DATEDIFF(year,
s.BOD,GETDATE()) as Umur,s.salary,
sum(o.amount) as Total_Amount
from salesperson as s
join orders as o on s.id = o.salesperson_id
where year(o.order_date) >= 2020
group by s.nama,DATEDIFF(year,
s.BOD,GETDATE()),s.salary
order by DATEDIFF(year,
s.BOD,GETDATE()) asc

--No.d
select s.nama,AVG(o.amount) as Rata_Rata
from salesperson as s
join orders as o on s.id = o.salesperson_id
group by s.nama
order by AVG(o.amount) desc

--No.e
select s.nama,
count(o.salesperson_id)as total_order,
sum(o.amount) as total_amount,
	case 
		when count(o.salesperson_id) > 2 and sum(o.amount) > 1000
		then (s.salary*0.3)
		else 0
		end bonus,
(s.salary+(s.salary*0.3)) as Total_Gaji
from salesperson as s
join orders as o on s.id = o.salesperson_id
group by s.nama,s.salary
having count(o.salesperson_id) > 2

--No.f
select s.nama
from salesperson as s 
left join orders as o on s.id = o.salesperson_id
where o.salesperson_id is null

--No.g
select s.nama , (s.salary-(s.salary*0.02)) as Gaji_Seteleh_Dipotong
from salesperson as s
left join orders as o on s.id = o.salesperson_id
where o.salesperson_id is null 




